package au.csiro.theme;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.themes.BasicTheme;

public class CsiroTheme extends BasicTheme {

	private static final Logger logger = LoggerFactory.getLogger(CsiroTheme.class);
	
	private static final Random r = new Random();
	
	public CsiroTheme() {
		logger.error("Created");
	}
	
	public boolean shouldShowBreadcrumbs() {
		logger.error("shouldShowBreadcrumbs");
		if (r.nextBoolean())
			throw new NullPointerException();
		return true;
	}

}
